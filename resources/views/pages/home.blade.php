@extends('layout')

@section('content')

    <div class="jumbotron">
        <h1>Project Flyer</h1>
        <p>

        </p>
    </div>

    @if ($signedIn)
        <a href="/flyers/create" class="btn btn-primary">Create a Flyer</a>
    @else
        <a href="/auth/register" class="btn btn-primary">Sign Up</a>
    @endif
@stop