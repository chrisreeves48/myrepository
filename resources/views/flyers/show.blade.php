@extends('layout')

@section('content')

    <div class="row">
        <div class="col-sm-4">
        <h1>{{ $flyer->street }}</h1>
        <h2>{!! $flyer->price !!}</h2>

    <hr>

            <div class="description">{!! nl2br($flyer->description) !!}</div>
        </div>


        <div class="col-sm-8 gallery">
            <hr>
            @foreach ($flyer->photos->chunk(4) as $set)
                <div class="row">
                    @foreach ($set as $photo)
                        <div class="col-sm-3 gallery__img">
                            <a href="/{{ $photo->path }}" data-lity>
                                <img src="/{{ $photo->thumbnail_path }}" alt="">
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach

                @if ($user && $user->owns($flyer))
                <hr>
                    {!! Form::open([
             'route'    => ['store.photos', $flyer->zip, $flyer->street],
             'method'   => 'POST',
             'files'    => true,
             'class'    => 'dropzone',
             'id'       => 'addPhotosForm'])
            !!}
                    {!! Form::close() !!}
                @endif
        </div>
    </div>

@stop

@section('scripts.footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script>
        Dropzone.options.addPhotosForm ={
            paramName: 'photo',
            maxFilesize: '4',
            acceptedFiles: '.jpg, .jpeg, .png, .bmp'
        };
    </script>
@stop
