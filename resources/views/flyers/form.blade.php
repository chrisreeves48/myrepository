@inject('countries', 'App\Http\Utilities\Country')


<div class="row">
    <div class="col-sm-6">

        {!! Form::token() !!}

        <div class="form-group">
            {!! Form::label('street','Street') !!}
            {!! Form::text('street', null, ['class' => 'form-control geocomplete']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('city','City') !!}
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('zip','Zip-Code') !!}
            {!! Form::text('zip', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('country') !!}
            {!! Form::select('country', ($countries::all()), 'US', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('state') !!}
            {!! Form::text('state', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('price', 'Sales Price') !!}

            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Home Description') !!}

            {!! Form::textarea('description', null,
            ['class' => 'form-control',
             'size' => '30x10'])
             !!}
        </div>

    </div>

    <div class="col-sm-12">
        <hr>
        <div class="form-group">
            {!! Form::submit('Create Flyer', array('class' => 'btn btn-primary')) !!}
        </div>
    </div>



</div>

@section('scripts')
    @parent
    @include('flyers.partials.geocodeScript', ['selector' => 'form'])
@stop



