@inject('countries', 'App\Http\Utilities\Country')
@extends('layout')


@section('content')

    <h1>Selling Your Home?</h1>


    {!! Form::open(['route' => 'flyers.store', 'method' => 'POST', 'files' => true]) !!}
    @include('flyers.form')

    @include('errors')
    {!! Form::close() !!}
@stop

