<script src="//maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/geocomplete/1.6.5/jquery.geocomplete.min.js"></script>

<script>
    $(function () {
        var form = $("{{ $selector }}");
        $(".geocomplete", form).geocomplete({
            types: ''
        }).bind("geocode:result", function(event, result) {
            var streetNo, route, city, state, zip;

            for (var i = 0; i < result.address_components.length; i++) {
                var component = result.address_components[i];
                var type = component.types[0];
                switch(type) {
                    case 'street_number':
                        streetNo = component.short_name;
                        break;
                    case 'route':
                        route = component.short_name;
                        break;
                    case 'locality':
                        city = component.short_name;
                        break;
                    case 'administrative_area_level_1':
                        state = component.long_name;
                        break;
                    case 'postal_code':
                        zip = component.short_name;
                        break;
                }
            }

            $("#street", form).val(streetNo + ' ' + route);
            $("#city", form).val(city);
            $("#state", form).val(state);
            $("#zip", form).val(zip);
        });
    });
</script>