@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">

            <h1>Register</h1>

            <hr>

            {!! Form::open(['url' => '/' . 'auth' . '/'. 'register', 'method' => 'POST' ]) !!}

            <div class="form-group">
                {!! Form::label('name') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('email', 'Email Address') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('password') !!}
                {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password_confirmation','Confirm Password', 'required') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::submit('Register', array('class' => 'btn btn-primary')) !!}
            </div>


            {!! Form::close() !!}

            @include('errors')

        </div>
    </div>

@stop