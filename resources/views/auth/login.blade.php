@extends('layout')

@section('content')
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">

     {!! Form::open(['url' => '/' . 'auth' . '/'. 'login', 'method' => 'POST' ]) !!}

    <div class="form-group">
        {!! Form::label('email') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password') !!}
        {!! Form::password('password', ['class' => 'form-control', 'required']) !!}
    </div>

    <div class="form-group">
        {!! Form::checkbox('remember', 'yes') !!}Remember Me
    </div>
    
    <div class="form-group">
        {!! Form::submit('Login', array('class' => 'btn btn-primary')) !!}
    </div>

     {!! Form::close() !!}

            @include('errors')

     </div>
    </div>

@stop