<?php
namespace App;

use App\AddPhotoToFlyer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Mockery as m;


/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 11/23/2015
 * Time: 6:57 PM
 */
class AddPhotoToFlyerTest extends \TestCase
{
    function it_processes_a_form_to_add_a_photo_to_a_flyer()
    {
        $flyer = factory(Flyer::class)->create();

        $file = m::mock(UploadedFile::class, [
          'getClientOriginalName' => 'foo',
          'getClientOriginalExtension' => 'jpg'
        ]);

        $file->shouldReceive('move')
          ->once()
          ->with('pics/photos', 'nowfoo.jpg');

        $thumbnail = m::mock(Thumbnail::class);

        $thumbnail->shouldReceive('make')
          ->once()
          ->with('pics/photos/nowfoo.jpg', 'pics/photos/tn-nowfoo.jpg');

        $form = new AddPhotoToFlyer($flyer, $file, $thumnail);

        $form->save();

        $this->assertCount(1, $flyer->photos);
    }

    function time()
    {
        return 'now';
    }

}
