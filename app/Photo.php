<?php

namespace App;

use Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Photo extends Model
{

    /**
     * The associated table.
     *
     * @var string
     */
    protected $table = 'flyers_photos';

    /**
     * Fillable Fields for a photo.
     *
     * @var array
     */
    protected $fillable = ['path', 'name', 'thumbnail_path'];


    /**
     * A photo belongs to a flyer
     *
     * @return \Illuminate\Database\Elequent\Relations\BelongsTo
     */
    public function flyer()
    {
        return $this->belongsTo('App\Flyer');
    }

    /**
     * Ge the base directory for photo uploads
     *
     * @return string
     */
    public function baseDir()
    {
        return 'pics/photos';
    }

    public  function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;

        $this->path = $this->baseDir() . '/' .$name;
        $this->thumbnail_path = $this->baseDir() . '/tn-' .$name;

    }

}