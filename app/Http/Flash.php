<?php
namespace App\Http;

class flash{

    //Master function for all other flash function to call.
    public  function create($title, $message, $level, $key='flash_message')
    {
        session()->flash($key, [
          'title'     => $title,
          'message'   => $message,
          'level'     => $level
        ]);
    }


    // when this function is called it gives the alert the 'info' icon
    public function info($title, $message)
    {
        return $this->create($title, $message, 'info');
    }


    // when this function is called it gives the alert the 'success' icon
    public function  success($title, $message)
    {
        return $this->create($title, $message, 'success');
    }


    // when this function is called it gives the alert the 'error' icon
    public function  error($title, $message)
    {
        return $this->create($title, $message, 'error');
    }


    // This function is for requiring a confirmation button to be pressed to proceed past the alert message.
    public function  overlay($title, $message, $level = 'success')
    {
        return $this->create($title, $message, $level, 'flash_message_overlay');
    }

}


